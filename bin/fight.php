#!/bin/env php
<?php declare(strict_types = 1);

namespace DaveRandom\Fight;

use DaveRandom\Fight\Graphics\Image;
use DaveRandom\Fight\Graphics\ImageManipulator;
use DaveRandom\Fight\Graphics\Rectangle;
use DaveRandom\Fight\Graphics\TtfFontWriter;

require __DIR__ . '/../src/bootstrap.php';

error_reporting(~0);
ini_set('display_errors', '1');

$templateImage = Image::createFromPath(RESOURCES_ROOT . '/template.png');
$player1ImageRect = new Rectangle(56, 104, 188, 189);
$player1TextRect = new Rectangle(47, 317, 208, 24);
$player2ImageRect = new Rectangle(356, 104, 188, 189);
$player2TextRect = new Rectangle(347, 317, 208, 24);
$template = new Template($templateImage, $player1ImageRect, $player1TextRect, $player2ImageRect, $player2TextRect, 5.0);

$player1 = new Player(Image::createFromPath(RESOURCES_ROOT . '/Patrick.jpg'), 'Patrick');
$player2 = new Player(Image::createFromPath(RESOURCES_ROOT . '/PeeHaa.png'), 'PeeHaa');

$imageGenerator = new ImageGenerator($template, new ImageManipulator, new TtfFontWriter(RESOURCES_ROOT . '\\SFNewRepublic-BoldItalic.ttf'));

$result = $imageGenerator->generateImage($player1, $player2);

\imagepng($result->getGdResource(), DATA_ROOT . '/result.png');
