<?php declare(strict_types = 1);

namespace DaveRandom\Fight;

use DaveRandom\Fight\Graphics\Color;
use DaveRandom\Fight\Graphics\HorizontalAlignment;
use DaveRandom\Fight\Graphics\Image;
use DaveRandom\Fight\Graphics\ImageManipulator;
use DaveRandom\Fight\Graphics\Rectangle;
use DaveRandom\Fight\Graphics\Size;
use DaveRandom\Fight\Graphics\TtfFontWriter;
use DaveRandom\Fight\Graphics\VerticalAlignment;

class ImageGenerator
{
    private $template;
    private $imageManipulator;
    private $fontWriter;

    private $degradationSize;

    public function __construct(Template $template, ImageManipulator $imageManipulator, TtfFontWriter $fontWriter)
    {
        $this->template = $template;
        $this->imageManipulator = $imageManipulator;
        $this->fontWriter = $fontWriter;

        $degradationDivisor = $this->template->getDegradationDivisor();

        if ($degradationDivisor !== 1.0) {
            $size = $this->template->getImage()->getSize();

            $this->degradationSize = new Size(
                (int)($size->getWidth() / $degradationDivisor),
                (int)($size->getHeight() / $degradationDivisor)
            );
        }
    }

    private function createResizedImage(Image $src, Size $dstSize): Image
    {
        $dst = new Image($dstSize);

        $this->imageManipulator->copyImageResampled($src, $dst);

        return $dst;
    }

    private function createPlayerImage(Image $srcImage, Size $targetSize): Image
    {
        if ($this->degradationSize !== null) {
            $srcImage = $this->createResizedImage($srcImage, $this->degradationSize);
        }

        return $this->createResizedImage($srcImage, $targetSize);
    }

    private function drawCenteredText(Image $image, string $text, float $size, Rectangle $bounds)
    {
        $boxSize = $this->fontWriter->measureString($text, $size);
        $box = $bounds->getAlignedInnerRectangle($boxSize, HorizontalAlignment::CENTER | VerticalAlignment::MIDDLE);
        $this->fontWriter->drawString($image, $text, $size, $box, new Color(255, 255, 255));
    }

    public function generateImage(Player $player1, Player $player2): Image
    {
        $player1Image = $this->createPlayerImage($player1->getImage(), $this->template->getPlayer1ImageRectangle()->getSize());
        $player2Image = $this->createPlayerImage($player2->getImage(), $this->template->getPlayer2ImageRectangle()->getSize());

        $result = clone $this->template->getImage();

        $this->imageManipulator->copyImage($player1Image, $result, null, $this->template->getPlayer1ImageRectangle()->getPosition());
        $this->imageManipulator->copyImage($player2Image, $result, null, $this->template->getPlayer2ImageRectangle()->getPosition());

        $this->drawCenteredText($result, \strtoupper($player1->getName()), 14.0, $this->template->getPlayer1TextRectangle());
        $this->drawCenteredText($result, \strtoupper($player2->getName()), 14.0, $this->template->getPlayer2TextRectangle());

        return $result;
    }
}
