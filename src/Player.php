<?php declare(strict_types = 1);

namespace DaveRandom\Fight;

use DaveRandom\Fight\Graphics\Image;

final class Player
{
    private $image;
    private $name;

    public function __construct(Image $image, string $name)
    {
        $this->image = $image;
        $this->name = $name;
    }

    public function getImage(): Image
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
