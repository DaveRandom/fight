<?php declare(strict_types = 1);

const APP_ROOT = __DIR__ . '/..';
const DATA_ROOT = __DIR__ . '/../data';
const RESOURCES_ROOT = __DIR__ . '/../resources';

require __DIR__ . '/../vendor/autoload.php';
