<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

class TtfFontWriter
{
    private $fontFilePath;

    public function __construct(string $fontFilePath)
    {
        $this->fontFilePath = $fontFilePath;
    }

    public function measureString(string $string, float $size): Size
    {
        $result = \imagettfbbox($size, 0, $this->fontFilePath, $string);

        if ($result === false) {
            throw new \RuntimeException('Getting bounding box for text failed');
        }

        return new Size($result[2], $result[3]);
    }

    /**
     * @param Image $image
     * @param string $string
     * @param float $size
     * @param Position|Rectangle $position
     * @param Color $color
     */
    public function drawString(Image $image, string $string, float $size, $position, Color $color): void
    {
        // gd works from the bottom left, we work from top left, so adjust the position accordingly
        if ($position instanceof Position) {
            $position = new Position(
                $position->getX(),
                $position->getY() + $this->measureString($string, $size)->getHeight()
            );
        } else if ($position instanceof Rectangle) {
            $position = new Position($position->getX(), $position->getY() + $position->getHeight());
        } else {
            throw new \InvalidArgumentException("Invalid position identifier");
        }

        $result = \imagettftext(
            $image->getGdResource(),
            $size, 0,
            $position->getX(), $position->getY(),
            $image->getColorIndex($color),
            $this->fontFilePath,
            $string#
        );

        if ($result === false) {
            throw new \RuntimeException('Drawing text failed');
        }
    }
}
