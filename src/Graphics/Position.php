<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

final class Position
{
    private $x;
    private $y;

    public function __construct($x, int $y = null)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }
}
