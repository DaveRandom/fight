<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

final class Rectangle
{
    private $position;
    private $size;

    /**
     * @param int|Position $x
     * @param int|Size $y
     * @param int $width
     * @param int $height
     */
    public function __construct($x, $y, $width = null, $height = null)
    {
        if ($x instanceof Position && $y instanceof Size) {
            $this->position = $x;
            $this->size = $y;
        } else if (\is_int($x) && \is_int($y) && \is_int($width) && \is_int($height)) {
            $this->position = new Position($x, $y);
            $this->size = new Size($width, $height);
        } else {
            throw new \InvalidArgumentException('Invalid combination of arguments to construct ' . self::class);
        }
    }

    public function getX(): int
    {
        return $this->position->getX();
    }

    public function getY(): int
    {
        return $this->position->getY();
    }

    public function getWidth(): int
    {
        return $this->size->getWidth();
    }

    public function getHeight(): int
    {
        return $this->size->getHeight();
    }

    public function getPosition(): Position
    {
        return $this->position;
    }

    public function getSize(): Size
    {
        return $this->size;
    }

    public function contains(self $rectangle): bool
    {
        return $this->position->getX() <= $rectangle->getX()
            && $this->position->getY() <= $rectangle->getX()
            && ($this->position->getX() + $this->size->getWidth()) >= ($rectangle->getX() + $rectangle->getWidth())
            && ($this->position->getY() + $this->size->getHeight()) >= ($rectangle->getY() + $rectangle->getHeight());
    }

    public function getAlignedInnerRectangle(Size $size, int $alignment = HorizontalAlignment::CENTER | VerticalAlignment::MIDDLE): self
    {
        if ($size->getHeight() > $this->size->getHeight() || $size->getWidth() > $this->size->getWidth()) {
            throw new \OutOfBoundsException('Inner size overflows outer rectangle');
        }

        if ($alignment & HorizontalAlignment::LEFT) {
            $x = $this->position->getX();
        } else if ($alignment & HorizontalAlignment::RIGHT) {
            $x = $this->position->getX() + ($this->size->getWidth() - $size->getWidth());
        } else {
            $x = $this->position->getX() + (int)(($this->size->getWidth() / 2) - ($size->getWidth() / 2));
        }

        if ($alignment & VerticalAlignment::TOP) {
            $y = $this->position->getY();
        } else if ($alignment & VerticalAlignment::BOTTOM) {
            $y = $this->position->getY() + ($this->size->getHeight() - $size->getHeight());
        } else {
            $y = $this->position->getY() + (int)(($this->size->getHeight() / 2) - ($size->getHeight() / 2));
        }

        return new self(new Position($x, $y), $size);
    }
}
