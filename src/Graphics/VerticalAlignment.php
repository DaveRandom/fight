<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

final class VerticalAlignment
{
    public const TOP    = 0b001 << 3;
    public const MIDDLE = 0b010 << 3;
    public const BOTTOM = 0b100 << 3;

    private function __construct() { }
}
