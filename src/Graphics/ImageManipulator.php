<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

class ImageManipulator
{
    public function copyImage(Image $src, Image $dst, Rectangle $srcRectangle = null, Position $dstPosition = null): void
    {
        $srcRectangle = $srcRectangle ?? new Rectangle(new Position(0, 0), $src->getSize());
        $dstPosition = $dstPosition ?? new Position(0, 0);

        $success = \imagecopy(
            $dst->getGdResource(), $src->getGdResource(),
            $dstPosition->getX(), $dstPosition->getY(),
            $srcRectangle->getX(), $srcRectangle->getY(),
            $srcRectangle->getWidth(), $srcRectangle->getHeight()
        );

        if (!$success) {
            throw new \RuntimeException('Copy operation failed');
        }
    }

    public function copyImageResampled(Image $src, Image $dst, Rectangle $srcRectangle = null, Rectangle $dstRectangle = null): void
    {
        $srcRectangle = $srcRectangle ?? new Rectangle(new Position(0, 0), $src->getSize());
        $dstRectangle = $dstRectangle ?? new Rectangle(new Position(0, 0), $dst->getSize());

        $success = \imagecopyresampled(
            $dst->getGdResource(), $src->getGdResource(),
            $dstRectangle->getX(), $dstRectangle->getY(),
            $srcRectangle->getX(), $srcRectangle->getY(),
            $dstRectangle->getWidth(), $dstRectangle->getHeight(),
            $srcRectangle->getWidth(), $srcRectangle->getHeight()
        );

        if (!$success) {
            throw new \RuntimeException('Resampling copy operation failed');
        }
    }
}
