<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

final class Color
{
    private $r;
    private $g;
    private $b;
    private $a;
    private $hex;

    public function __construct(int $r, int $g, int $b, int $a = 0)
    {
        $this->r = $r & 0xff;
        $this->g = $g & 0xff;
        $this->b = $b & 0xff;
        $this->a = $a & 0xff;
    }

    public function getR(): int
    {
        return $this->r;
    }

    public function getG(): int
    {
        return $this->g;
    }

    public function getB(): int
    {
        return $this->b;
    }

    public function getA(): int
    {
        return $this->a;
    }

    public function getHex(): string
    {
        return $this->hex
            ?? $this->hex = \sprintf('%02x%02x%02x%02x', $this->a, $this->r, $this->g, $this->b);
    }
}
