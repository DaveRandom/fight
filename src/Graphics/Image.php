<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

final class Image
{
    private $size;
    private $gdResource;

    private static function createGdResourceFromImageData(string $data)
    {
        if (false === $gdResource = \imagecreatefromstring($data)) {
            throw new \RuntimeException('Loading data as image failed');
        }

        return $gdResource;
    }

    private function getSizeFromGdResource($gdResource): Size
    {
        if (false === $width = \imagesx($gdResource)) {
            throw new \RuntimeException('Retrieving width of image failed');
        }

        if (false === $height = \imagesy($gdResource)) {
            throw new \RuntimeException('Retrieving height of image failed');
        }

        return new Size($width, $height);
    }

    private static function createGdResourceFromSize(Size $size)
    {
        if (false === $gdResource = \imagecreatetruecolor($size->getWidth(), $size->getHeight())) {
            throw new \RuntimeException('Creating empty image failed');
        }

        return $gdResource;
    }

    public static function createFromPath(string $path): self
    {
        if (!\file_exists($path)) {
            throw new \InvalidArgumentException("{$path} does not exist");
        }

        if (!\is_file($path)) {
            throw new \InvalidArgumentException("{$path} is not a file");
        }

        if (!\is_readable($path)) {
            throw new \InvalidArgumentException("{$path} is not readable");
        }

        if (false === $data = \file_get_contents($path)) {
            throw new \RuntimeException("Reading file '{$path}' failed");
        }

        return new self($data);
    }

    /**
     * @param string|resource|Size $data
     */
    public function __construct($data)
    {
        if (\is_resource($data)) {
            $this->gdResource = $data;
        } else if (\is_string($data)) {
            $this->gdResource = self::createGdResourceFromImageData($data);
        } else if ($data instanceof Size) {
            $this->gdResource = self::createGdResourceFromSize($data);
        } else {
            throw new \InvalidArgumentException('Invalid argument for ' . self::class);
        }

        $this->size = self::getSizeFromGdResource($this->gdResource);
    }

    public function __clone()
    {
        $dst = self::createGdResourceFromSize($this->size);
        $interlace = $this->isInterlaced();

        if (!\imagecopy($dst, $this->gdResource, 0, 0, 0, 0, $this->size->getWidth(), $this->size->getHeight())) {
            throw new \RuntimeException('Copy operation failed');
        }

        $this->gdResource = $dst;
        $this->setInterlaced($interlace);
    }

    public function getSize(): Size
    {
        return $this->size;
    }

    /**
     * @return resource
     */
    public function getGdResource()
    {
        return $this->gdResource;
    }

    public function isInterlaced(): bool
    {
        if (false === $result = \imageinterlace($this->gdResource)) {
            throw new \RuntimeException('Retrieving interlace bit failed');
        }

        return (bool)$result;
    }

    public function setInterlaced(bool $interlace): void
    {
        if (false === \imageinterlace($this->gdResource, (int)$interlace)) {
            throw new \RuntimeException('Setting interlace bit failed');
        }
    }

    public function getColorIndex(Color $color): int
    {
        $result = \imagecolorallocatealpha($this->gdResource, $color->getR(), $color->getG(), $color->getB(), (int)($color->getA() / 2));

        if ($result === false) {
            throw new \RuntimeException('Allocating color failed');
        }

        return $result;
    }
}
