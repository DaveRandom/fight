<?php declare(strict_types = 1);

namespace DaveRandom\Fight\Graphics;

final class HorizontalAlignment
{
    public const LEFT   = 0b001;
    public const CENTER = 0b010;
    public const RIGHT  = 0b100;

    private function __construct() { }
}
