<?php declare(strict_types = 1);

namespace DaveRandom\Fight;

use DaveRandom\Fight\Graphics\Image;
use DaveRandom\Fight\Graphics\Position;
use DaveRandom\Fight\Graphics\Rectangle;

class Template
{
    private $image;
    private $player1ImageRectangle;
    private $player1TextRectangle;
    private $player2ImageRectangle;
    private $player2TextRectangle;
    private $degradationDivisor;

    public function __construct(
        Image $image,
        Rectangle $player1ImageRectangle,
        Rectangle $player1TextRectangle,
        Rectangle $player2ImageRectangle,
        Rectangle $player2TextRectangle,
        float $degradationMultiplier = 1.0
    ) {
        $imageRectangle = new Rectangle(new Position(0, 0), $image->getSize());

        if (!$imageRectangle->contains($player1ImageRectangle)) {
            var_dump($imageRectangle, $player1ImageRectangle);
            throw new \InvalidArgumentException('Player 1 image rectangle outside template image bounds');
        }

        if (!$imageRectangle->contains($player2ImageRectangle)) {
            throw new \InvalidArgumentException('Player 2 image rectangle outside template image bounds');
        }

        $this->image = $image;
        $this->player1ImageRectangle = $player1ImageRectangle;
        $this->player1TextRectangle = $player1TextRectangle;
        $this->player2ImageRectangle = $player2ImageRectangle;
        $this->player2TextRectangle = $player2TextRectangle;
        $this->degradationDivisor = $degradationMultiplier;
    }

    public function getImage(): Image
    {
        return $this->image;
    }

    public function getPlayer1ImageRectangle(): Rectangle
    {
        return $this->player1ImageRectangle;
    }

    public function getPlayer1TextRectangle(): Rectangle
    {
        return $this->player1TextRectangle;
    }

    public function getPlayer2ImageRectangle(): Rectangle
    {
        return $this->player2ImageRectangle;
    }

    public function getPlayer2TextRectangle(): Rectangle
    {
        return $this->player2TextRectangle;
    }

    public function getDegradationDivisor(): float
    {
        return $this->degradationDivisor;
    }
}
